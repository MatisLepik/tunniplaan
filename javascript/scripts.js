var timeTable = new TimeTable("#outputTable");
var totalClasses = -1;
var dayArray;
var classData;

$(document).on('click', 'td', function (evt) {
    $(evt.currentTarget).find('.classteacher').toggleClass('hidden');
});

$.post('getTunniplaan.php', function (data) {
    extractData(data);
    fillTimeTable();
    timeTable.updateTable();
    contentLoaded();
});

function contentLoaded() {
    $('.ico_loading').addClass('loaded');
    $('table').addClass('loaded');
}

function extractData(data) {
    var html = $.parseHTML(data, document, true);

    //Extract javascript code that contains positioning information
    var scriptData = html[8].children[0].children[4].innerHTML;
    //Extract relevant part to speed up subsequent queries:
    var scriptBeginning = scriptData.search(/#plan_div_1/);
    var relevantScript = scriptData.substr(scriptBeginning, scriptData.length);

    //Extract day index from script into array
    var dayData = relevantScript.match(/left................/g);
    for (var i = 0; i < dayData.length; i++) {
        dayData[i] = dayData[i][dayData[i].length - 1];
    }

    dayArray = dayData;
    totalClasses = dayData.length;

    classData = html[8].children[0].children[5];
}

function fillTimeTable() {
    for (var i = 1; i <= totalClasses; i++) {

        var contents = $(classData).find('#plan_div_' + i)[0];
        var time = contents.children[0].title;
        var title = contents.children[0].innerText;
        var teacher = contents.children[2].innerText;
        var room = contents.children[4].innerText;

        var classNumber;
        for (var timeCount = 1; timeCount <= 10; timeCount++) {
            if (time == timeTable.classTimes[timeCount]) {
                classNumber = timeCount;
            }
        }
        var classString = '<span class="classname">' + title + '</span><span class="classroom">' + room + '</span><span class="classteacher hidden">' + teacher + '</span>';

        switch (dayArray[i - 1]) { //dayArray is zero-based
        case "1": //mon
            timeTable.days.monday.classes[classNumber] = classString;
            break;
        case "2": //tue
            timeTable.days.tuesday.classes[classNumber] = classString;
            break;
        case "3": //wed
            timeTable.days.wednesday.classes[classNumber] = classString;
            break;
        case "4": //thu
            timeTable.days.thursday.classes[classNumber] = classString;
            break;
        case "5": //fri
            timeTable.days.friday.classes[classNumber] = classString;
            break;
        }
    }
}
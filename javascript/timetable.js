function TimeTable(table) {
    this.classCount = 10;
    this.targetTable = $(table);
    this.tableBody = $(table + "> tbody");

    this.days = {
        monday: {
            name: "Esmaspäev",
            classes: {}
        },
        tuesday: {
            name: "Teisipäev",
            classes: {}
        },
        wednesday: {
            name: "Kolmapäev",
            classes: {}
        },
        thursday: {
            name: "Neljapäev",
            classes: {}
        },
        friday: {
            name: "Reede",
            classes: {}
        }
    };
    this.classTimes = {
        1:"08:15-09:00",
        2:"09:10-09:55",
        3:"10:05-10:50",
        4:"11:00-11:45",
        5:"12:30-13:15",
        6:"13:25-14:10",
        7:"14:20-15:05",
        8:"15:15-16:00",
        9:"16:10-16:55",
        10:"17:05-17:50",
    }

    /* @constructor
     * Creates empty timeslots for each day
     */
    var keys = Object.keys(this.days);
    var keysLength = keys.length;

    for (var dayNr = 0; dayNr < keysLength; dayNr++) {

        var day = this.days[keys[dayNr]].classes;
        for (var classNr = 1; classNr <= this.classCount; classNr++) {
            day[classNr] = "";
        }
    }
}
TimeTable.prototype.updateTable = function () {
    //Create a table body if it doesn't exist
    if (this.tableBody.length == 0) {
        this.targetTable.append('<tbody></tbody>');
        this.tableBody = this.targetTable.find('tbody');
    }
    
    //Empty table body
    this.tableBody.empty();
    
    //Fill body based on the timetable
    for (var classNr = 1; classNr <= this.classCount; classNr++) {
        var row = '<tr>';
        
        //Time column
        row += '<td><span class="classnr">' + classNr + '</span><span class="classtime">' + this.classTimes[classNr] + '</span></td>';

        var keys = Object.keys(this.days);
        var keysLength = keys.length;
        for (var dayNr = 0; dayNr < keysLength; dayNr++) {
            row += '<td>' + this.days[keys[dayNr]]['classes'][classNr] + '</td>';
        }

        row += '</tr>';
        this.tableBody.append(row);
    }
};
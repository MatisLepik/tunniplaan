'use strict';

module.exports = function (grunt) {

    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({

        // watch for changes and trigger compass, jshint, uglify and livereload
        watch: {
            compass: {
                options: {
                    spawn: true
                },
                files: ['css/scss/{,*/}*.scss', 'css/scss/**/*.scss'],
                tasks: ['compass:dev', 'autoprefixer']
            },
            livereloadCss: {
                options: {
                    livereload: true
                },
                files: [
                    'css/output.css'
                ]
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: [
                    '**/*.html',
                    '**/*.php',
                    'images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                    'scripts/output.js'
                ]
            }
        },
        
        // compass and scss
        compass: {
            dist: {
                options: {
                    sassDir: 'css/scss',
                    cssDir: 'css',
                    generatedImagesDir: 'images/generated',
                    imagesDir: 'images',
                    javascriptsDir: 'scripts',
                    fontsDir: 'fonts',
                    httpImagesPath: '/images',
                    httpGeneratedImagesPath: '/images/generated',
                    httpFontsPath: '/fonts',
                    relativeAssets: false
                }
            },
            dev: {
                options: {
                    sassDir: 'css/scss',
                    cssDir: 'css',
                    generatedImagesDir: 'images/generated',
                    imagesDir: 'images',
                    javascriptsDir: 'scripts',
                    fontsDir: 'fonts',
                    httpImagesPath: '/images',
                    httpGeneratedImagesPath: '/images/generated',
                    httpFontsPath: '/fonts',
                    relativeAssets: false
                }
            }
        },

        // Add vendor prefixed css
        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie >= 8', '> 1%']
            },
            files: {
                expand: true,
                cwd: 'css',
                src: 'main.css',
                dest: 'css'
            }
        }
    });

    // register tasks

    grunt.registerTask('dev', function (target) {
        grunt.task.run([
            'compass:dev',
            'autoprefixer'
        ]);

        if (target === 'watch') {
            return grunt.task.run([
                'watch'
            ]);
        }
    });
};

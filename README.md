### Tunniplaan ###

I created this site to help keep track of what classes we have in school. The [original version](https://siseveeb.ee/tpt/veebivormid/tunniplaan/grupid?nadal=$startofweek&oppegrupp=126) provided by the school is not very good, so my version parses the html content of that site and creates a better version from scratch. It's a bit slow, because there is no API, so it has to download and chew through the original file, which is quite large.

You can click on any cell to bring up extra information about that class. 

**You can see the [live version](http://matislepik.eu/tunniplaan/) on my website.**

### Contact ###

- Matis Lepik

- matis.lepik@gmail.com

- www.matislepik.eu